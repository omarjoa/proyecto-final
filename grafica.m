% Load the data from our text file
data = load('datosomar.txt');
% Define x and y
x = data(:,2);
y = data(:,1);
% Create a function to plot the data
function plotData(x,y)
plot(x,y,'rx','MarkerSize',8); % Plot the data
end
% Plot the data
plotData(x,y);
xlabel('Relacion Calificaciones - Horas de Sueño'); % Set the x-axis label
ylabel('Horas Sueño'); % Set the y-axis label
fprintf('Program paused. Press enter to continue.\n');